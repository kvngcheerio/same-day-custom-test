module.exports = {
  theme: {
    backgroundColor: theme => ( {
      ...theme( 'colors' ),
      'primary': '#0A1F44',
      'gray-custom': '#F7F8F9',
    } ),
    borderColor: theme => ( {
      ...theme( 'colors' ),
      'custom-gray': '#F1F2F4',
      'gray-200': '#E1E4E8',
    } ),
    extend: {
      fontSize: {
        tiny: "10px"
      },
      textColor: {
        'primary': '#0A1F44',
      },
    }
  },
  variants: {},
  plugins: []
}
