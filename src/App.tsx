import React from "react";
import { Router} from "react-router";
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Routes from "./common/components/Routes";
import history from "./common/config/history";

declare var location: Location;

const App = () => (

	<BrowserRouter>
		<Switch>
			<Routes />
		</Switch>
	</BrowserRouter>
);

export default App;
