import { combineReducers } from "redux";
import sessionReducer from "../../modules/dashboard/reducer";


export default combineReducers({
	user: sessionReducer,
});
