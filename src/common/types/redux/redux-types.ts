import {
	EmptyLoggedInUserDataModel,
	LoggedInUserStoreModel,
} from "../../../modules/dashboard/types/session-model";

export class ReduxActionModel {
	type: string = "";
	payload: any = {};
}

export type ReduxStoreModel = {
	user: LoggedInUserStoreModel | EmptyLoggedInUserDataModel;
};
