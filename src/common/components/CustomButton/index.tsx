import { LoadingOutlined } from "@ant-design/icons";
import { Button } from "antd";
import { ButtonHTMLType } from "antd/lib/button/button";
import React, { CSSProperties } from "react";

interface ICustomButtonProps {
	actionText: string;
	disabled?: boolean;
	action?: any;
	loading?: boolean;
	block?: boolean;
	loaded?: boolean;
	size?: "small" | "middle" | "large";
	style?: CSSProperties;
	type?: ButtonHTMLType;
}

export function CustomButton({
	actionText,
	loading = false,
	disabled = false,
	loaded = false,
	block = false,
	action = () => {},
	size = "middle",
	style = {},
	type = "button",
}: ICustomButtonProps) {
	return (
		<Button
			size={size}
			disabled={loading || disabled}
			htmlType={type}
			onClick={action}
			style={style}
			block={block}
			className="btn btn-default flex justify-center"
		>
			{actionText}
			{loading && <LoadingOutlined spin />}
			{/* {!loading && loaded && (
				<CheckOutlined
					className="turn-around"
					style={{ fontSize: 20, width: 25 }}
				/>
			)} */}
		</Button>
	);
}
