import { DatePicker } from "antd";
import { Moment } from "moment";
import * as React from "react";
import CalendarIcon from "../CustomIcons/CalendarIcon";

export interface ICustomDatePickerProps {
	disabledDate: (current: Moment) => boolean;
	onChange: (event: any) => void;
	initialValue?: Moment;
}

export default function CustomDatePicker({
	disabledDate,
	onChange,
	initialValue,
}: ICustomDatePickerProps) {
	return (
		<DatePicker
			defaultValue={initialValue}
			className="full-width"
			format={(value) => value.format("DD MMM YYYY")}
			onChange={onChange}
			disabledDate={disabledDate}
			style={{ borderRadius: 5 }}
			suffixIcon={<CalendarIcon />}
			placeholder="Choose date"
		/>
	);
}
