import React from "react";

export default function ExternalLinkIcon(props: {
	height?: number;
	width?: number;
}) {
	return (
		<svg
			height={props.height ? props.height : "17"}
			width={props.width ? props.width : "16"}
			viewBox="0 0 17 16"
			fill="none"
			style={{ display: "inline" }}
		>
			<path
				d="M12.6016 8.66667V12.6667C12.6016 13.0203 12.4611 13.3594 12.211 13.6095C11.961 13.8595 11.6219 14 11.2682 14H3.9349C3.58127 14 3.24214 13.8595 2.99209 13.6095C2.74204 13.3594 2.60156 13.0203 2.60156 12.6667V5.33333C2.60156 4.97971 2.74204 4.64057 2.99209 4.39052C3.24214 4.14048 3.58127 4 3.9349 4H7.9349"
				stroke="#4E5D78"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				d="M10.6016 2H14.6016V6"
				stroke="#4E5D78"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				d="M7.26953 9.33333L14.6029 2"
				stroke="#4E5D78"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	);
}
