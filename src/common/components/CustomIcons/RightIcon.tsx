import Icon from "@ant-design/icons";
import React from "react";

export default function RightIcon(props: { height?: number; width?: number }) {
	return (
		<Icon
			component={() => (
				<svg
					height={props.height ? props.height : "7"}
					width={props.width ? props.width : "12"}
					viewBox="0 0 7 12"
					fill="none"
					style={{ display: "inline" }}
				>
					<path
						d="M1 1.33464L5.66667 6.0013L1 10.668"
						stroke="#B0B7C3"
						strokeWidth="2"
						strokeLinecap="round"
						strokeLinejoin="round"
					/>
				</svg>
			)}
		/>
	);
}
