import React from "react";

export default function GlobeCustomIcon(props: {
	height?: number;
	width?: number;
}) {
	return (
		<svg
			height={props.height ? props.height : "16"}
			width={props.width ? props.width : "16"}
			viewBox="0 0 16 16"
			fill="none"
			style={{ display: "inline" }}
		>
			<path
				d="M8.00065 14.6654C11.6825 14.6654 14.6673 11.6806 14.6673 7.9987C14.6673 4.3168 11.6825 1.33203 8.00065 1.33203C4.31875 1.33203 1.33398 4.3168 1.33398 7.9987C1.33398 11.6806 4.31875 14.6654 8.00065 14.6654Z"
				stroke="#B0B7C3"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				d="M1.33398 8H14.6673"
				stroke="#B0B7C3"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				d="M8.00065 1.33203C9.66817 3.1576 10.6158 5.52672 10.6673 7.9987C10.6158 10.4707 9.66817 12.8398 8.00065 14.6654C6.33313 12.8398 5.38548 10.4707 5.33398 7.9987C5.38548 5.52672 6.33313 3.1576 8.00065 1.33203V1.33203Z"
				stroke="#B0B7C3"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	);
}
