import React from "react";

export default function ClockIcon({
	height = 20,
	width = 20,
	color = "#4E5D78",
}: {
	height?: number;
	width?: number;
	color?: string;
}) {
	return (
		<svg
			height={height}
			width={width}
			viewBox="0 0 20 20"
			fill="none"
			style={{ display: "inline" }}
		>
			<path
				d="M9.99935 18.3346C14.6017 18.3346 18.3327 14.6037 18.3327 10.0013C18.3327 5.39893 14.6017 1.66797 9.99935 1.66797C5.39698 1.66797 1.66602 5.39893 1.66602 10.0013C1.66602 14.6037 5.39698 18.3346 9.99935 18.3346Z"
				stroke={color}
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				d="M10 5V10L13.3333 11.6667"
				stroke={color}
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	);
}
