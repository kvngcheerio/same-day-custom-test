import React from "react";

export default function CopyCustomIcon(props: {
	height?: number;
	width?: number;
}) {
	return (
		<svg
			height={props.height ? props.height : "16"}
			width={props.width ? props.width : "16"}
			viewBox="0 0 16 16"
			fill="none"
			style={{ display: "inline" }}
		>
			<path
				d="M13.3333 6H7.33333C6.59695 6 6 6.59695 6 7.33333V13.3333C6 14.0697 6.59695 14.6667 7.33333 14.6667H13.3333C14.0697 14.6667 14.6667 14.0697 14.6667 13.3333V7.33333C14.6667 6.59695 14.0697 6 13.3333 6Z"
				stroke="#B0B7C3"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				d="M3.33398 9.9987H2.66732C2.3137 9.9987 1.97456 9.85822 1.72451 9.60817C1.47446 9.35813 1.33398 9.01899 1.33398 8.66536V2.66536C1.33398 2.31174 1.47446 1.9726 1.72451 1.72256C1.97456 1.47251 2.3137 1.33203 2.66732 1.33203H8.66732C9.02094 1.33203 9.36008 1.47251 9.61013 1.72256C9.86017 1.9726 10.0007 2.31174 10.0007 2.66536V3.33203"
				stroke="#B0B7C3"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	);
}
