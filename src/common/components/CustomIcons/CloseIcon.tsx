import React from "react";

export default function CloseIcon(props: { height?: number; width?: number }) {
	return (
		<svg
			height={props.height ? props.height : "12"}
			width={props.width ? props.width : "12"}
			viewBox="0 0 12 12"
			fill="none"
			style={{ display: "inline" }}
		>
			<path
				d="M1 1L11 11M1 11L11 1L1 11Z"
				stroke="#B0B7C3"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	);
}
