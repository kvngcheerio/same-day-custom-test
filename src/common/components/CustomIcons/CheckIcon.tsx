import React from "react";

export default function CheckIcon(props: { height?: number; width?: number }) {
	return (
		<svg
			height={props.height ? props.height : "16"}
			width={props.width ? props.width : "12"}
			viewBox="0 0 16 12"
			fill="none"
			style={{ display: "inline" }}
		>
			<path
				d="M1 7L5 11L15 1"
				stroke="#B0B7C3"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	);
}
