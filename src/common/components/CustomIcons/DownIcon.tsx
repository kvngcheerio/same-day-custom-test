import React from "react";

export default function DownIcon(props: { height?: number; width?: number }) {
	return (
		<svg
			height={props.height ? props.height : "12"}
			width={props.width ? props.width : "7"}
			viewBox="0 0 12 7"
			fill="none"
			style={{ display: "inline" }}
		>
			<path
				d="M10.6673 1L6.00065 5.66667L1.33398 1"
				stroke="#8A94A6"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	);
}
