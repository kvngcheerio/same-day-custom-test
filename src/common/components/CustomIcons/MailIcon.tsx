import React from "react";

export default function MailIcon(props: { height?: number; width?: number }) {
	return (
		<svg
			height={props.height ? props.height : "20"}
			width={props.width ? props.width : "20"}
			viewBox="0 0 20 20"
			fill="none"
			style={{ display: "inline" }}
		>
			<path
				d="M3.33464 3.33203H16.668C17.5846 3.33203 18.3346 4.08203 18.3346 4.9987V14.9987C18.3346 15.9154 17.5846 16.6654 16.668 16.6654H3.33464C2.41797 16.6654 1.66797 15.9154 1.66797 14.9987V4.9987C1.66797 4.08203 2.41797 3.33203 3.33464 3.33203Z"
				stroke="#B0B7C3"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				d="M18.3346 5L10.0013 10.8333L1.66797 5"
				stroke="#B0B7C3"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	);
}
