import Icon from "@ant-design/icons";
import React from "react";

export default function CreditCardIcon({
	height = 20,
	width = 20,
	color = "#8A94A6",
}: {
	height?: number;
	width?: number;
	color?: string;
}) {
	return (
		<Icon
			component={() => (
				<svg
					height={height}
					width={width}
					viewBox="0 0 20 20"
					fill="none"
					style={{ display: "inline" }}
				>
					<g clipPath="url(#clip0)">
						<path
							d="M17.5006 3.33203H2.50065C1.58018 3.33203 0.833984 4.07822 0.833984 4.9987V14.9987C0.833984 15.9192 1.58018 16.6654 2.50065 16.6654H17.5006C18.4211 16.6654 19.1673 15.9192 19.1673 14.9987V4.9987C19.1673 4.07822 18.4211 3.33203 17.5006 3.33203Z"
							stroke={color}
							strokeWidth="2"
							strokeLinecap="round"
							strokeLinejoin="round"
						/>
						<path
							d="M0.833984 8.33203H19.1673"
							stroke={color}
							strokeWidth="2"
							strokeLinecap="round"
							strokeLinejoin="round"
						/>
					</g>
					<defs>
						<clipPath id="clip0">
							<rect width="20" height="20" fill="white" />
						</clipPath>
					</defs>
				</svg>
			)}
		/>
	);
}
