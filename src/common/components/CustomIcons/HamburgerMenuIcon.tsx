import React from "react";

export default function HamburgerMenuIcon(props: {
	height?: number;
	width?: number;
}) {
	return (
		<svg
			height={props.height ? props.height : "20"}
			width={props.width ? props.width : "20"}
			viewBox="0 0 20 20"
			fill="none"
			style={{ display: "inline" }}
		>
			<path
				d="M2.5 10H17.5"
				stroke="#B0B7C3"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				d="M2.5 5H17.5"
				stroke="#B0B7C3"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
			<path
				d="M2.5 15H17.5"
				stroke="#B0B7C3"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
			/>
		</svg>
	);
}
