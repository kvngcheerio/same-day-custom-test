import { CheckCircleFilled } from "@ant-design/icons";
import { Radio } from "antd";
import * as React from "react";

export interface ICustomRadioButtonProps {
	value: string;
	label: string;
	selectedValue: string;
}

export default function CustomRadioButton({
	value,
	label,
	selectedValue,
}: ICustomRadioButtonProps) {
	return (
		<Radio.Button
			style={{ borderRadius: 5 }}
			className="flex flex-row justify-between py-1"
			value={value}
		>
			<div
				style={{ height: "100%" }}
				className="flex flex-row justify-between align-items-center content"
			>
				<span
					className={`text ${selectedValue === value ? "selected-label" : ""}`}
				>
					{label}
				</span>
				{selectedValue === value && (
					<CheckCircleFilled style={{ fontSize: 16 }} />
				)}
			</div>
		</Radio.Button>
	);
}
