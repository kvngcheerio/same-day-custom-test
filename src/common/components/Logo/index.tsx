import * as React from "react";
import logo from "../../../static/images/logo.png";

export interface ILogoProps {}

export default function Logo(props: ILogoProps) {
	return <img alt="same-day" style={{ width: 100, height: 30}} src={logo} />;
}
