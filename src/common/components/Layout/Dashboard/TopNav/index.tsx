import { Avatar, Dropdown, Menu, message, Button } from "antd";
import React from "react";
import history from "../../../../config/history";
import Logo from "../../../Logo";
import user from "../../../../../static/images/icons/user.png"
import caret from "../../../../../static/images/icons/caret.png"
import chat from "../../../../../static/images/icons/Chat.png"

export interface TopNavProps {
	activeLink: "labs" | "products";
}

const TopNav: React.FunctionComponent<TopNavProps> = ({ activeLink }) => {
	
	return (
		<div className="bg-primary py-2 px-5 md:px-10 full-width flex flex-row align-items-center justify-between top-nav">
			<div className="top-left flex flex-row">
				<div className="mt10">
				<Logo />
				</div>
				
				<div
					key="labs"
					className={`text-white pl-6 link-item ml-20 ${
						activeLink === "labs" ? " active" : ""
					}`}
					onClick={() => history.push("/")}
				>
					Design Lab
				</div>
				<div
					key="labs"
					className={`text-white pl-6 link-item ml-20`}
					onClick={() => history.push("/")}
				>
					Products
				</div>

				<div
					key="labs"
					className={`text-white pl-6 link-item ml-20`}
					onClick={() => history.push("/")}
				>
					How It Works
				</div>

				<div
					key="labs"
					className={`text-white pl-6 link-item ml-20`}
					onClick={() => history.push("/")}
				>
					Bulk Pricing
				</div>
			</div>

			<div className="top-right flex flex-row">
			<Button className="btn btn-grey inline-block">
            <img style={{ width: 13, height: 13, marginRight:5}} src={chat}/>
            <span>
            Get Help
            </span>
			<img style={{ width: 13, height: 13, marginRight:5}} src={caret}/>
            </Button>
			<Button className="btn btn-grey inline-block">
            <img style={{ width: 13, height: 13, marginRight:5}} src={user}/>
            <span>
            Your Account
            </span>
			<img style={{ width: 13, height: 13, marginRight:5}} src={caret}/>
            </Button>
			</div>
		</div>
	);
};

export default TopNav;
