import { lazy } from "react";
import { RouteConfig } from "./types";

/**  function to lazy-load routes */
const loadModules = (link: string) =>
	lazy(() => import(`../../../modules/${link}`));

export const routes: RouteConfig[] = [
	{
		path: "/",
		Component: loadModules("dashboard/pages/DesignPage"),
		access: "guest-only",
		exact: true,
	},
	{
		path: "/",
		Component: loadModules("404"),
		access: "guest-only",
		exact: false,
	},
];
