export interface RoutesProps {
	setCurrentPageIndex?: Function;
}

export interface RouteConfig {
	path: string;
	access: "everyone" | "guest-only" | "business-only";
	exact: boolean;
	Component: any;
}
