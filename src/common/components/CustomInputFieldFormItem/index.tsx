import { Form, Input } from "antd";
import { Rule } from "antd/lib/form";
import React, { CSSProperties, ReactElement } from "react";
interface ICustomInputFieldFormItemProps {
	placeholder?: string;
	id?: string;
	label?: string;
	type?: string;
	initialValue?: any;
	size?: "small" | "middle" | "large";
	suffix?: string | ReactElement;
	prefix?: string | ReactElement;
	min?: number;
	max?: number;
	customInputContainerStyles?: CSSProperties;
	CustomInputFieldFormItemStyles?: CSSProperties;
	InputComponent?: ReactElement;
	fieldName: string;
	rules?: Rule[];
}

export const CustomInputFieldFormItem = ({
	id = "",
	placeholder = "",
	label = "",
	type = "text",
	size = "middle",
	suffix,
	prefix,
	min,
	max,
	InputComponent,
	initialValue,
	customInputContainerStyles,
	CustomInputFieldFormItemStyles,
	rules,
	fieldName,
}: ICustomInputFieldFormItemProps) => {
	return (
		<>
			<div className="custom-input" style={customInputContainerStyles}>
				{InputComponent ? (
					<Form.Item
						initialValue={initialValue}
						label={label}
						name={fieldName}
						rules={rules}
					>
						{InputComponent}
					</Form.Item>
				) : (
					<Form.Item
						initialValue={initialValue}
						label={label}
						name={fieldName}
						rules={rules}
					>
						<Input
							min={min}
							max={max}
							size={size}
							id={id}
							type={type}
							placeholder={placeholder}
							prefix={prefix}
							suffix={suffix}
							style={CustomInputFieldFormItemStyles}
						/>
					</Form.Item>
				)}
			</div>
		</>
	);
};
