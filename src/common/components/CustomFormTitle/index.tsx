import * as React from "react";

export interface ICustomFormTitleProps {
	title: string;
}

export default function CustomFormTitle({ title }: ICustomFormTitleProps) {
	return (
		<b
			className="mb-3"
			style={{
				fontSize: 20,
				lineHeight: "150%",
				fontWeight: 500,
			}}
		>
			{title}
		</b>
	);
}
