const Language = {
	NetworkErrorMessage: {
		errorMessage: "Network Error",
	},
	PageLoaderComponent: { loadingText: "loading" },
	SuspenseFallback: {
		fallbackText: "loading",
	},
};

export default Language;
