import { Button } from "antd";
import * as React from "react";
import history from "../../common/config/history";
import _404_image from "../../static/images/404.png";

export interface I_404Props {}

export default function _404(props: I_404Props) {
	return (
		<div className="full-screen-height full-screen-width flex flex-col align-items-center justify-content-center">
			<img
				style={{ width: 200, height: 200 }}
				src={_404_image}
				alt="not found"
			/>
			<div className="text-xl mb-1">There’s nothing out here.</div>
			<div className="text-sm font-light mb-5">
				Sorry, we couldn’t seem to find the page you’re looking for. <br />
				Not to worry, let’s get you right back to our homepage.
			</div>
			<div>
				<Button
					onClick={() => history.push("/login")}
					className="btn btn-default inline-block"
				>
					Go back home
				</Button>
			</div>
		</div>
	);
}
