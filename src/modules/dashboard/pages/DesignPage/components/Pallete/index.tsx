import { Avatar, Dropdown, Menu, message } from "antd";
import React from "react";
import ShirtLarge from "../../../../../../static/images/svg/shirt-large.svg";
import undo from "../../../../../../static/images/svg/undo.svg";
import redo from "../../../../../../static/images/svg/redo.svg";
import shirtsim from "../../../../../../static/images/shirt-sim.png";
import rightsleeve from "../../../../../../static/images/rightSleeve.png";
import leftsleeve from "../../../../../../static/images/leftSleeve.png";
import plus from "../../../../../../static/images/svg/plus.svg";
import minus from "../../../../../../static/images/svg/minus.svg";


export interface PalleteProps {
    activeLink: "front" | "back" | "right-sleeve" | "leftsleeve";
}

const Pallete: React.FunctionComponent<PalleteProps> = ({activeLink}) => {
return (
    <div className="pallete flex flex-row">
        <div className="left">
        <div className="action">
        <img style={{ width: 20, height: 20 }} src={undo}/>
        <p>Undo</p>

        <img style={{ width: 20, height: 20 }} src={redo}/>
        <p>Redo</p>
        </div>
        </div>


<div className="center">
<img  style={{ width: '100%', height: '80vh' }} src={ShirtLarge}/>
</div>

<div className="right">
<div className="sim">
    <div className={`body ${activeLink === "front" ? " active" : ""}`}>
    <img style={{ width: 40, height: 40 }} src={shirtsim}/>
        <p>Front</p>
    </div>
        
    <div className="body">
        <img style={{ width: 40, height: 40 }} src={shirtsim}/>
        <p>Back</p>
        </div>

        <div className="body">
        <img style={{ width: 20, height: 40 }} src={rightsleeve}/>
        <p>Right Sleeve</p>
        </div>

        <div className="body">
        <img style={{ width: 20, height: 40 }} src={leftsleeve}/>
        <p>Left Sleeve</p>
        </div>
        </div>

        <div className="zoom">
            <div className="icons">
            <img style={{ width: 20, height: 20 }} src={plus}/>
            </div>

            <div className="icons">
            <img style={{ width: 20, height: 20 }} src={minus}/>
                </div>
        </div>
        </div>
        </div>

)

};
    
export default Pallete;