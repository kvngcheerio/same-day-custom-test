import * as React from "react";
import text from "../../../../../../../../static/images/svg/Text.svg";

export interface ITextProps {}

export default function Text(props: ITextProps) {
	return <img alt="same-day-text" style={{ width: 30, height: 30 }} src={text} />;
}
