import * as React from "react";
import note from "../../../../../../../../static/images/svg/note.svg";

export interface INoteProps {}

export default function Note(props: INoteProps) {
	return <img alt="same-day-text" style={{ width: 30, height: 30 }} src={note} />;
}
