import * as React from "react";
import team from "../../../../../../../../static/images/svg/team.svg";

export interface ITeamProps {}

export default function Team(props: ITeamProps) {
	return <img alt="same-day-text" style={{ width: 30, height: 30 }} src={team} />;
}
