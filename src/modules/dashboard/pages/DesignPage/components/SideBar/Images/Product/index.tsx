import * as React from "react";
import product from "../../../../../../../../static/images/svg/product.svg";

export interface IProductProps {}

export default function Product(props: IProductProps) {
	return <img alt="same-day-text" style={{ width: 30, height: 30 }} src={product} />;
}
