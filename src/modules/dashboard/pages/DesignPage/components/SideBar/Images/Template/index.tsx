import * as React from "react";
import template from "../../../../../../../../static/images/svg/template.svg";

export interface ITemplateProps {}

export default function Template(props: ITemplateProps) {
	return <img alt="same-day-text" style={{ width: 30, height: 30 }} src={template} />;
}
