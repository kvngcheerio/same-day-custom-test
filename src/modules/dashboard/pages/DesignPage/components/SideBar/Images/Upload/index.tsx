import * as React from "react";
import upload from "../../../../../../../../static/images/svg/upload.svg";

export interface IUploadProps {}

export default function Upload(props: IUploadProps) {
	return <img alt="same-day-text" style={{ width: 30, height: 30 }} src={upload} />;
}
