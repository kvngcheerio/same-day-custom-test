import { Avatar, Dropdown, Menu, message } from "antd";
import React from "react";
import Note from "./Images/Note";
import Product from "./Images/Product";
import Team from "./Images/Team";
import Template from "./Images/Template";
import Text from "./Images/Text";
import Upload from "./Images/Upload";

import upload from "../../../../../../static/images/svg/upload.svg";
import uploadd from "../../../../../../static/images/svg/upload-dark.svg";
import text from "../../../../../../static/images/svg/Text.svg";
import note from "../../../../../../static/images/svg/note.svg";
import team from "../../../../../../static/images/svg/team.svg";
import template from "../../../../../../static/images/svg/template.svg";
import product from "../../../../../../static/images/svg/product.svg";
// import Text from "../../../../../static/svg/text.png";
// console.log(Text);

export interface SideBarProps {
	activeLink: "add-text" | "use-template";
}

const SideBar: React.FunctionComponent<SideBarProps> = ({ activeLink }) => {
	
	return (
        <div className="side-bar">
			<div className="left align-items-center">
            <div className={`link-item ${
						activeLink === "add-text" ? " active" : ""
					}`}>
                       <img  style={{ width: 30, height: 30 }} src={text}/>
                       <br/>
            <span>Add Text</span>
				</div>
               

                <div className="link-item">
                <img  style={{ width: 30, height: 30 }} src={template}/>
                <br/>
            <span>Use Template</span>
				</div>
                 <div className="link-item">
                <img style={{ width: 30, height: 30 }} src={upload}/>
                <br/>
            <span>Upload Design</span>
				</div>

                <div className="link-item">
                <img style={{ width: 30, height: 30 }} src={product}/>
                <br/>
            <span>Product Colors</span>
				</div>

                <div className="link-item">
                <img style={{ width: 30, height: 30 }} src={team}/>
                <br/>
            <span>Add Team Names</span>
				</div>

                <div className="link-item">
                <img style={{ width: 30, height: 30 }} src={note}/>
                <br/>
            <span>Add Notes</span>
				</div> 

             
               

            </div>
            <div className="right">

            </div>
            </div>
        );
    };
    
export default SideBar;