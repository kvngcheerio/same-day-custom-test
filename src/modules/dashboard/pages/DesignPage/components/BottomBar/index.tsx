import { Avatar, Dropdown, Menu, message, Button } from "antd";
import React from "react";
import Logo from "../../../../../../common/components/Logo";
import shirt from "../../../../../../static/images/shirt.png"
import cicleplus from "../../../../../../static/images/svg/circle-plus.svg";
import share from "../../../../../../static/images/svg/share.svg";
import save from "../../../../../../static/images/svg/save.svg";
import dollar from "../../../../../../static/images/svg/dollar.svg"

// import Text from "../../../../../static/svg/text.png";


export interface BottomBarProps {
}

const BottomBar: React.FunctionComponent<BottomBarProps> = () => {

    return (
        <div className="full-width flex flex-row bottom-nav">
            <div className="left flex flex-row">
            <div className="item">
            <Button className="btn btn-light inline-block">
            <img style={{ width: 16, height: 16, marginRight:5 }} src={cicleplus}/>
            Add Products
            </Button>
            </div>
            
            <div className="item-d">
            <img style={{ width: 40, height: 40 }} src={shirt}/>
            </div>
            <div className="item-s">
            <span>Gildan Unisex T-shirt <a>Change Product</a></span><br/>
            <span>White <a>Change Color</a></span>
            </div>

            </div>
           
            <div className="right flex flex-row">
            <div className="item">
            <Button className="btn btn-light inline-block">
                
            <img style={{ width: 15, height: 15}} src={share}/>
            </Button>
            </div>
            <div className="item">
            <Button className="btn btn-light inline-block">
            <img style={{ width: 13, height: 13, marginRight:5 }} src={save}/>
            Save
            </Button>
            </div>
            <div className="item">

                
            <Button className="btn btn-default inline-block">
            <img style={{ width: 13, height: 13, marginRight:5}} src={dollar}/>
            <span>
            Get Pricing
            </span>
            </Button>
            </div>
            </div>
        </div>

        );
    };
    
export default BottomBar;