import Icon from "@ant-design/icons";
import { Col, notification, Row, message} from "antd";
import * as React from "react";
import { useDispatch } from "react-redux";
import history from "../../../../common/config/history";
import { DashboardShell, Loader } from "../../../../common/components";
import TopNav from "../../../../common/components/Layout/Dashboard/TopNav";
import SideBar from "./components/SideBar";
import BottomBar from "./components/BottomBar";
import Pallete from "./components/Pallete";

export interface IDesignPageProps {}

export default function DesignPage(props: IDesignPageProps) {
	const dispatch = useDispatch();



	return (
		<DashboardShell>
			<div
				className="justify-between py-5 px-5 md:px-10"
				style={{
					position: "fixed",
					top: 0,
					left: 0,
					right: 0,
					backgroundColor: "rgba(255, 255, 255, 0.95)",
					zIndex: 2,
				}}
			>
				<TopNav activeLink="labs" />
				<div className="partial-height full-width flex flex-row">
				<div className="left-width">
				<SideBar activeLink="add-text"/>
				</div>
				<div className="right-width">
					<Pallete activeLink="front"/>

				</div>
				</div>

				<BottomBar/>
				
				</div>
				
		</DashboardShell>
	);
}
