export type LoggedInUserDetails = {
    token: string|null,
    user: {}| null
};

export type LoggedOutUserDetails = {
	loggedIn: boolean;
	email: string;
};
export class EmptyLoggedInUserDataModel {
	loading: boolean = false;
	error: null = null;
	data: LoggedInUserDetails = { token: null, user: null };
}

export type LoggedInUserStoreModel = {
	loading: boolean;
	error: string | null;
	data: {};
};
